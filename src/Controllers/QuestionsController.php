<?php

namespace Ucc\Controllers;

use Ucc\Services\QuestionService;
use Ucc\Http\JsonResponseTrait;
use Ucc\Session;

class QuestionsController extends Controller
{
    use JsonResponseTrait;

    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
    }

    public function beginGame()
    {
        $name = $this->requestBody->name ?? null;
        if (empty($name)) {
            return $this->json('You must provide a name', 400);
        }

        Session::start();
        Session::set('name', $name);
        Session::set('questionCount', 1);
        $questions = $this->questionService->getRandomQuestions(1);
        $question = array_shift($questions);

        return $this->json(['question' => $question->jsonSerialize()], 201);
    }

    public function answerQuestion(int $id): bool
    {
        if (Session::get('name') === null) {
            return $this->json('You must first begin a game', 400);
        }

        if ((int)Session::get('questionCount') > 4) {
            $name = Session::get('name');
            $points = Session::get('points');
            Session::destroy();
            return $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            return $this->json('You must provide an answer', 400);
        }

        //TODO: Check answer and increment user's points. Reply with a proper message
        $question = $this->questionService->getQuestion($id);
        $points = $this->questionService->getPointsForAnswer($question, $answer);
        $sumPoints = ((int) Session::get('points') ?? 0) + 1;
        Session::set('points', $sumPoints);
        $message = 'Well done!';

        $questions = $this->questionService->getRandomQuestions(1, $id);
        $newQuestion = array_shift($questions);

        return $this->json(['message' => $message, 'question' => $newQuestion]);
    }
}
