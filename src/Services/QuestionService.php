<?php

namespace Ucc\Services;

use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
    }

    public function loadQuestions(): array
    {
        $jsonQuestions = file_get_contents(QuestionService::QUESTIONS_PATH);
        return json_decode($jsonQuestions);
    }

    public function getAllQuestions(): array
    {
        $jsonDecodedQuestions = $this->loadQuestions();

        $questions = [];
        foreach ($jsonDecodedQuestions as $jsonQuestion) {
            $questions[] = $this->jsonMapper->map($jsonQuestion, new Question);
        }

        return $questions;
    }

    /**
     * @param integer $count
     * @return []Question
     */
    public function getRandomQuestions(int $count = 5, $except = null): array
    {
        //TODO: Get {$count} random questions from JSON
        $jsonDecodedQuestions = $this->loadQuestions();
        shuffle($jsonDecodedQuestions);

        $questions = [];
        foreach ($jsonDecodedQuestions as $key => $question) {
            if ($key >= $count) {
                break;
            } else if ($except && $question->id === $except) {
                continue;
            }
            $questions[] = $this->jsonMapper->map($question, new Question);
            $count++;
        }

        return $questions;
    }

    public function getQuestion(int $id): ?Question
    {
        $jsonDecodedQuestions = $this->loadQuestions();

        foreach ($jsonDecodedQuestions as $key => $question) {
            if ($question->id === $id) {
                return $this->jsonMapper->map($question, new Question);
            }
        }
    }

    public function getPointsForAnswer(Question $question, string $answer): int
    {
        if ($question->getCorrectAnswer() == $answer) {
            return $question->getPoints();
        }
    }
}
